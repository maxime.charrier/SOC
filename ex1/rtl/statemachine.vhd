----------------------------------------------------------------------------------
-- Company: HEIA - FR
-- Engineer: Dr. Maxime Charri�re
-- 
-- Create Date:    16:36:11 02/25/2016 
-- Design Name: 	S�rie 01
-- Module Name:    statemachine - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
-- use IEEE.NUMERIC_STD.ALL;

-- Package contenant fonction complement2()
-- ainsi que les d�clarations de composants
use work.two_s_complement_pkg.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity statemachine is
	port ( 
		clk_i   : in std_logic;
		reset_i : in std_logic;
		
		start_i : in std_logic;
		port_i  : in std_logic_vector (7 downto 0);
		
		rdy_o      : out std_logic;
		load_in_o  : out std_logic;
		load_out_o : out std_logic;
		
		port_o  : out std_logic_vector(7 downto 0)
	);
end statemachine;

architecture Behavioral of statemachine is

	--Type
	type state_type is (idle, read_in, write_out);	
	signal next_state, present_state : state_type;
	
begin

	--State machine - next state logic
	ProcessNextStateLogic: process(present_state, start_i)
	begin
		case present_state is
			when idle =>		
				if (start_i = '1') then		
					next_state <= read_in;
				end if;
			when read_in =>
				next_state <= write_out;
			when write_out =>
				next_state <= idle;
		end case;
	end process ProcessNextStateLogic;

	-- current state logic
	ProcessCurrentStateLogic: process(clk_i, reset_i)
	begin
		if (reset_i = '1') then
			present_state <= idle;
		elsif rising_edge(clk_i) then
			present_state <= next_state;
		end if;
	end process ProcessCurrentStateLogic;
	
	-- process output logic	
	ProcessOutputLogic : process(present_state)
	begin
		case present_state is
			when idle =>
				rdy_o <= '1';
				load_in_o <= '0';
				load_out_o <= '0';
				
			when read_in =>
				rdy_o <= '0';
				load_in_o <= '1';
				load_out_o <= '0';
	
			when write_out =>
				rdy_o <= '0';
				load_in_o <= '0';
				load_out_o <= '1';										
		end case;	
	end process ProcessOutputLogic;
	
	-- 2 complements
	port_o <= complement2(port_i);
	
end Behavioral;

