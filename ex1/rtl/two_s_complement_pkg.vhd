library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.ALL;

package two_s_complement_pkg is

	function complement2 (input : std_logic_vector) return std_logic_vector;

	component register_8bits is
		port (
			clk_i   : in std_logic;
			reset_i : in std_logic;
			
			load_i : in std_logic;
			port_i : in std_logic_vector (7 downto 0);
			port_o : out std_logic_vector (7 downto 0)
		);
	end component;

	component statemachine is
		port ( 
			clk_i   : in std_logic;
			reset_i : in std_logic;
			
			start_i : in std_logic;
			port_i  : in std_logic_vector (7 downto 0);
			
			rdy_o      : out std_logic;
			load_in_o  : out std_logic;
			load_out_o : out std_logic;
			
			port_o  : out std_logic_vector(7 downto 0)
		);
	end component;
	
end two_s_complement_pkg;

package body two_s_complement_pkg is

	function complement2 (input : std_logic_vector) return std_logic_vector is
	begin
		return std_logic_vector((unsigned((not(input)))) + 1);
	end;
	
end two_s_complement_pkg;
