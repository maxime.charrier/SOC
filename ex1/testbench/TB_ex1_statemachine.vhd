--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   14:05:01 02/25/2016
-- Design Name:   
-- Module Name:   C:/Work/HEIA-FR/SOC/TP/ex1/testbench/TB_ex1_statemachine.vhd
-- Project Name:  ex1
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: ex1_statemachine
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
USE ieee.numeric_std.ALL;
 
ENTITY TB_ex1_statemachine IS
END TB_ex1_statemachine;
 
ARCHITECTURE behavior OF TB_ex1_statemachine IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT ex1_statemachine
    PORT(
         clk_i : IN  std_logic;
         reset_i : IN  std_logic;
         start_i : IN  std_logic;
         port_i : IN  std_logic_vector(7 downto 0);
			rdy_o  : OUT std_logic;
         port_o : OUT  std_logic_vector(7 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal clk_i : std_logic := '0';
   signal reset_i : std_logic := '0';
   signal start_i : std_logic := '0';
   signal port_i : std_logic_vector(7 downto 0):=(others=>'0');

 	--Outputs
   signal rdy_o : std_logic;
   signal port_o : std_logic_vector(7 downto 0);

   -- Clock period definitions
   constant clk_i_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: ex1_statemachine PORT MAP (
          clk_i => clk_i,
          reset_i => reset_i,
          start_i => start_i,
          port_i => port_i,
          rdy_o  => rdy_o,
          port_o => port_o
        );

   -- Clock process definitions
   clk_i_process :process
   begin
		clk_i <= '0';
		wait for clk_i_period/2;
		clk_i <= '1';
		wait for clk_i_period/2;
   end process;
 

   -- Reset process
   reset_proc: process
   begin		
      -- hold reset state for 50 ns.
		reset_i <= '1';
      wait for 50 ns;	
		reset_i <= '0';
      
		wait;
		
   end process;

   -- Stimulus process
   stim_proc: process
	variable incr : unsigned(7 downto 0):=x"00";
   begin		
		--Init
		start_i <= '0';
		
      wait for clk_i_period*10;

		--Run state-machine
		port_i <= std_logic_vector(incr);
		start_i <= '1';
      wait for clk_i_period;
		start_i <= '0';
		
		incr:=incr+1;

   end process;
	

END;
