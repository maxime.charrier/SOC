--
--	Package File Template
--
--	Purpose: This package defines supplemental types, subtypes, 
--		 constants, and functions 
--
--   To use any of the example code shown below, uncomment the lines and modify as necessary
--

library IEEE;
use IEEE.STD_LOGIC_1164.all;

package mypackage_pkg is

component counter 
	generic(
		g_CNTER_MAX : integer := 0
	);
	port ( 
		clk_i   : in  std_logic;
		reset_i : in  std_logic;

		start_i : in std_logic;
		
		flag_o : out std_logic
	);
end component;

component chenillard 
	generic(
		g_CNTER_MAX : integer := 10000000
	);
	port ( 
		clk_i   : in  std_logic;
		reset_i : in  std_logic;

		chg_btn_i : in std_logic;
		
		port_o : out std_logic_vector(7 downto 0)
	);
end component;

component rising_detect
	generic(
		g_CNTER_MAX : integer := 30000000
	);
	port (
		clk_i			: in std_logic;
		reset_i		: in std_logic;
		
		s_i	: in std_logic;
		s_o	: out std_logic
	);
end component;

end mypackage_pkg;

package body mypackage_pkg is
 
end mypackage_pkg;
