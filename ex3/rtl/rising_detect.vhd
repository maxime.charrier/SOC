----------------------------------------------------------------------------------
-- Company: 		CERN/TE/MSC/MM
-- Engineer: 		Daniel Oberson
-- 
-- Create Date:    17:25:22 01/22/2013 
-- Design Name: 
-- Module Name:    rising_detect - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity rising_detect is
	generic(
		g_CNTER_MAX : integer := 30000000
	);
	port (
		clk_i			: in std_logic;
		reset_i		: in std_logic;
		
		s_i	: in std_logic;
		s_o	: out std_logic
	);
end rising_detect;

architecture Behavioral of rising_detect is

	--Signal
	signal s_km1_s,flag_s	: std_logic;
	signal counter_s : unsigned(31 downto 0);
begin

	process (clk_i)
	begin
		if rising_edge(clk_i) then
			if (reset_i='1') then
				counter_s <= (others=>'0');
				s_km1_s 	<= '0';
				s_o		<= '0';
				flag_s   <= '0';
			else
				s_km1_s 	<= s_i;
				if (s_i='1' and s_km1_s='0' and flag_s='0') then
					s_o	<= '1';
					flag_s <= '1';
				else
					s_o	<= '0';
				end if;
				if (flag_s='1') then
					counter_s <= counter_s + 1;
				end if;
				if (to_integer(counter_s)>=g_CNTER_MAX) then				
					counter_s <= (others=>'0');
					flag_s <= '0';
				end if;
			end if;
		end if;
	end process;
	
end Behavioral;

