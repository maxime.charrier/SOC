----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    14:45:48 02/12/2016 
-- Design Name: 
-- Module Name:    ex3_top - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

Library UNISIM;
use UNISIM.vcomponents.all;

use work.mypackage_pkg.ALL;

entity ex3_top is
	Port ( 
		clk_i   : in  std_logic;
		reset_i : in  std_logic;

		chg_btn_i : in std_logic;
		
		port_o : out std_logic_vector(7 downto 0)
	);
end ex3_top;

architecture Behavioral of ex3_top is

	--Signals
	signal clk_feedback_s, pll_locked_s : std_logic;
	signal sys_clk_s,reset_s : std_logic;
	
	signal button_s : std_logic;
	
begin

   -- PLL
   -- PLL_BASE: Phase Locked Loop (PLL) Clock Management Component
   --           Spartan-6
   -- Xilinx HDL Language Template, version 14.7

   PLL_BASE_inst : PLL_BASE
   generic map (
      BANDWIDTH => "OPTIMIZED",             -- "HIGH", "LOW" or "OPTIMIZED" 
      CLKFBOUT_MULT => 10,                  -- Multiply value for all CLKOUT clock outputs (1-64)
      CLKFBOUT_PHASE => 0.0,                -- Phase offset in degrees of the clock feedback output
                                            -- (0.0-360.0).
      CLKIN_PERIOD => 0.0,                  -- Input clock period in ns to ps resolution (i.e. 33.333 is 30
                                            -- MHz).
      -- CLKOUT0_DIVIDE - CLKOUT5_DIVIDE: Divide amount for CLKOUT# clock output (1-128)
      CLKOUT0_DIVIDE => 10,
      CLKOUT1_DIVIDE => 1,
      CLKOUT2_DIVIDE => 1,
      CLKOUT3_DIVIDE => 1,
      CLKOUT4_DIVIDE => 1,
      CLKOUT5_DIVIDE => 1,
      -- CLKOUT0_DUTY_CYCLE - CLKOUT5_DUTY_CYCLE: Duty cycle for CLKOUT# clock output (0.01-0.99).
      CLKOUT0_DUTY_CYCLE => 0.5,
      CLKOUT1_DUTY_CYCLE => 0.5,
      CLKOUT2_DUTY_CYCLE => 0.5,
      CLKOUT3_DUTY_CYCLE => 0.5,
      CLKOUT4_DUTY_CYCLE => 0.5,
      CLKOUT5_DUTY_CYCLE => 0.5,
      -- CLKOUT0_PHASE - CLKOUT5_PHASE: Output phase relationship for CLKOUT# clock output (-360.0-360.0).
      CLKOUT0_PHASE => 0.0,
      CLKOUT1_PHASE => 0.0,
      CLKOUT2_PHASE => 0.0,
      CLKOUT3_PHASE => 0.0,
      CLKOUT4_PHASE => 0.0,
      CLKOUT5_PHASE => 0.0,
      CLK_FEEDBACK => "CLKFBOUT",           -- Clock source to drive CLKFBIN ("CLKFBOUT" or "CLKOUT0")
      COMPENSATION => "SYSTEM_SYNCHRONOUS", -- "SYSTEM_SYNCHRONOUS", "SOURCE_SYNCHRONOUS", "EXTERNAL" 
      DIVCLK_DIVIDE => 1,                   -- Division value for all output clocks (1-52)
      REF_JITTER => 0.1,                    -- Reference Clock Jitter in UI (0.000-0.999).
      RESET_ON_LOSS_OF_LOCK => FALSE        -- Must be set to FALSE
   )
   port map (
      CLKFBOUT => clk_feedback_s, -- 1-bit output: PLL_BASE feedback output
      -- CLKOUT0 - CLKOUT5: 1-bit (each) output: Clock outputs
      CLKOUT0 => sys_clk_s,
      CLKOUT1 => open,
      CLKOUT2 => open,
      CLKOUT3 => open,
      CLKOUT4 => open,
      CLKOUT5 => open,
      LOCKED => pll_locked_s,    	-- 1-bit output: PLL_BASE lock status output
      CLKFBIN => clk_feedback_s,   	-- 1-bit input: Feedback clock input
      CLKIN => clk_i,       			-- 1-bit input: Clock input
      RST => reset_i           	   -- 1-bit input: Reset input
   );
		
	--Chenillard
	cmp_chenillard : chenillard
	generic map(
		g_CNTER_MAX => 10 --pour simulations. Pour cas r�el : 7000000
	)
	port map( 
		clk_i   => sys_clk_s,
		reset_i => reset_s,

		chg_btn_i => button_s,
		
		port_o => port_o
	);
	
	--rebouclage
	

	--reset
	reset_s <= reset_i or not(pll_locked_s);

	--Monostable pour le bouton de changement de sens
	cmp_rising_detect : rising_detect
	generic map(
		g_CNTER_MAX => 10 --pour simulations. Pour cas r�el : 30000000
	)
	port map(
		clk_i   => sys_clk_s,
		reset_i => reset_s,
		
		s_i	=> chg_btn_i,
		s_o	=> button_s
	);
	
end Behavioral;

