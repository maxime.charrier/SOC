--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   15:18:31 03/22/2018
-- Design Name:   
-- Module Name:   C:/Users/daniel.oberson/switchdrive/Cours/B_SYPC(SYC2)/SOC/Ex/ex3 - Solution/testbench/TB_ex3_top.vhd
-- Project Name:  ex3
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: ex3_top
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY TB_ex3_top IS
END TB_ex3_top;
 
ARCHITECTURE behavior OF TB_ex3_top IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT ex3_top
    PORT(
         clk_i : IN  std_logic;
         reset_i : IN  std_logic;
         chg_btn_i : IN  std_logic;
         port_o : OUT  std_logic_vector(7 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal clk_i : std_logic := '0';
   signal reset_i : std_logic := '0';
   signal chg_btn_i : std_logic := '0';

 	--Outputs
   signal port_o : std_logic_vector(7 downto 0);

   -- Clock period definitions
   constant clk_i_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: ex3_top PORT MAP (
          clk_i   => clk_i,
          reset_i => reset_i,
			 
          chg_btn_i => chg_btn_i,
          port_o    => port_o
        );

   -- Clock process definitions
   clk_i_process :process
   begin
		clk_i <= '0';
		wait for clk_i_period/2;
		clk_i <= '1';
		wait for clk_i_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
		--Init
		reset_i <= '1';
		chg_btn_i <= '0';
		
      wait for clk_i_period*10;
		reset_i <= '0';

		--Wait until PLL is locked
      wait for clk_i_period*40;
				
		--Two pulse to high level to check the monostable
		chg_btn_i <= '1';
      wait for clk_i_period;
		chg_btn_i <= '0';
      wait for clk_i_period;
		chg_btn_i <= '1';
      wait for clk_i_period;
		chg_btn_i <= '0';

      wait;
   end process;

END;
