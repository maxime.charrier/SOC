----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    08:37:23 05/02/2018 
-- Design Name: 
-- Module Name:    registers - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

use work.miniproc_pkg.all;

entity registers is
	port (
		clk_i   : in std_logic;
		reset_i : in std_logic;
		
		input_bus_i : in std_logic_vector(7 downto 0);
		
		a_o : out std_logic_vector(7 downto 0);
		b_o : out std_logic_vector(7 downto 0);
		
		w_i : in std_logic_vector(1 downto 0);
		reg2_i : in std_logic_vector(1 downto 0);
		outB_i : in std_logic_vector(1 downto 0); 
		
		rw_i  : in std_logic;
		selaa_i : in std_logic         
	);
end registers;

architecture Behavioral of registers is

	type tRegister is array (0 to 3) of  std_logic_vector(input_bus_i'high downto 0);
	
	--Signals
	signal a_s : std_logic_vector(1 downto 0);
	signal register_s : tRegister := (others => (others=>'0')); -- It's not synthesizable.
	
begin

	--Code	
	
	--If rw_i = cWRITE, rewritten in the register 
	register_s(to_integer(unsigned(w_i))) <= input_bus_i when rw_i = cWRITE;
	
	--Selection between W and A
	a_s <= w_i when selaa_i = '1' else 
			 reg2_i when selaa_i = '0';
	
	--Transmits the necessary register values to the outputs
	process(clk_i, reset_i)
	begin
		if reset_i = '1' then
			a_o <= "00000000";
			b_o <= "00000000";
		elsif rising_edge(clk_i) then
			a_o <= register_s( to_integer(unsigned(a_s)) );
			b_o <= register_s( to_integer(unsigned(outB_i)) );			
		end if;
	end process;			 

end Behavioral;

