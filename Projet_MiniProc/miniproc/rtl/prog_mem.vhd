----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    14:33:54 11/02/2015 
-- Design Name: 
-- Module Name:    prog_mem - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.miniproc_pkg.all;

entity prog_mem is
	port (
		clk_i : in std_logic;
		adr_i : in std_logic_vector(10 downto 0);
		data_io : inout std_logic_vector(15 downto 0);
		ce_i : in std_logic;
		rw_i : in std_logic
	);
end prog_mem;

architecture Behavioral of prog_mem is

	type prog_mem_type is array(2048 downto 0) of std_logic_vector(15 downto 0);
	--program memory
	signal prog_mem_s : prog_mem_type := ( --    short program:
		0  => "0000000000000000",	--       LOAD 		R0,M[0]
		1  => "0000100000000001",	--       LOAD 		R1,M[1]
		2  => "0111100010000000",	--       ADD 		R3,R0,R1
		3  => "0011100000000011",	--       STORE		R3,M[3]
		4  => "0101001000000000",	--       MOVE 		R2,R1
		5  => "0000000000000011",	--       LOAD 		R0,M[3]
		6  => "0110000110000000",	--       ADD   	R0,R0,R3
		7  => "1000100000000000",	--       SHIFTL 	R1
		8  => "1010000010000000",	--       CMP 		R0,R1
		9  => "1100000000000010",	--       BR 		LOOP2
		10  => "1110111111111101",	--       JMP 		LOOP1
		11  => "1110000000000000",	--       JMP 		LOOP2		
		others => "0000000000000000"
	);
	signal adrint : std_logic_vector(4 downto 0);
	signal outint : std_logic_vector(15 downto 0);
	signal ceint :  std_logic;
	
begin

	adrint <= adr_i(4 downto 0);
	ceint <= '1' when ((ce_i='1') and (31>=to_integer(unsigned(adr_i)))) else '0';
	
	process (clk_i)
	begin
		if falling_edge(clk_i) then
			outint <= prog_mem_s(to_integer(unsigned(adrint)));
			if rw_i=cWRITE then
				if ceint='1' then
					prog_mem_s(to_integer(unsigned(adrint))) <= data_io;
				end if;
			end if;
		end if;
	end process;

	data_io <= outint when (ceint='1' and rw_i=cREAD) else (others => 'Z');
	
end Behavioral;

