----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    10:56:31 05/02/2018 
-- Design Name: 
-- Module Name:    controler - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

use work.miniproc_pkg.all;

entity controler is
	port (
		clk_i   : in std_logic;
		reset_i : in std_logic;
		
		flag_i   : in std_logic;
		opcode_i : in std_logic_vector(2 downto 0);
		
		rwmem_o : out std_logic;
		
		selaa_o : out std_logic;
		rwreg_o : out std_logic;
		opalu_o : out std_logic_vector(1 downto 0);
		inalu_o : out std_logic;
		inreg_o : out std_logic;
		oealu_o : out std_logic;
		ldres_o : out std_logic;
		ldpc_o  : out std_logic;
		selpc_o : out std_logic;
		ldir_o  : out std_logic
	);
end controler;

architecture Behavioral of controler is

	--Type
	type ctrl_state is(
		FETCH, DECOD, EXEC, MEM_ACCESS, WRITE_BACK
	);
	
	--Signal
	signal next_state, present_state : ctrl_state;

begin

	-- State Machine with 3 process
	
	-- next state logic
	ProcessNextStateLogic: process(present_state)
	begin
		case present_state is
			when FETCH =>		
				next_state <= DECOD;
			when DECOD =>
				next_state <= EXEC;
			when EXEC =>
				next_state <= MEM_ACCESS;
			when MEM_ACCESS =>
				next_state <= WRITE_BACK;
			when WRITE_BACK =>
				next_state <= FETCH;
		end case;
	end process ProcessNextStateLogic;

	-- current state logic
	ProcessCurrentStateLogic: process(clk_i, reset_i)
	begin
		if (reset_i = '1') then
			present_state <= FETCH;
		elsif rising_edge(clk_i) then
			present_state <= next_state;
		end if;
	end process ProcessCurrentStateLogic;
	
	-- process output logic	
	ProcessOutputLogic : process(present_state)
	begin
		case present_state is
		
			-- output values for FETCH state
			when FETCH =>
			
					-- initial output values
					rwmem_o <= cREAD;
					selaa_o <= '0';
					rwreg_o <= cREAD; 
					opalu_o <= "00";
					inalu_o <= '1';
					inreg_o <= '0';
					oealu_o <= '0';
					ldres_o <= '0';
					ldpc_o  <= '0';
					selpc_o <= '0';
					ldir_o  <= '1';		-- Loads the "Data Bus MemProg"
					
					-- Depending on each instruction		
					case opcode_i is
						when cLOAD_INSTR =>
						when cSTORE_INSTR =>
						when cMOVE_INSTR =>
						when cADD_INSTR =>
						when cSHIFTL_INSTR =>
						when cCMP_INSTR =>
						when cBR_INSTR =>
						when cJMP_INSTR =>
						when OTHERS =>
					end case;
					
			-- output values for DECOD state
			when DECOD =>
			
					-- Regardless of the instruction		
					rwmem_o <= cREAD;
					rwreg_o <= cREAD; 
					ldres_o <= '0';
					ldpc_o  <= '0';
					ldir_o  <= '0';	
					
					-- Depending on each instruction							
					case opcode_i is
						when cLOAD_INSTR =>
							inreg_o <= '1';		
						when cSTORE_INSTR =>
							opalu_o <= cADD;
							selaa_o <= '1';
							inalu_o <= '0';					
						when cMOVE_INSTR =>
							opalu_o <= cADD;
							inalu_o <= '0';	
						when cADD_INSTR =>
							opalu_o <= cADD;							
						when cSHIFTL_INSTR =>
							opalu_o <= cSHIFTL;
							selaa_o <= '1';
							inalu_o <= '0';							
						when cCMP_INSTR =>
							opalu_o <= cCMP;							
						when cBR_INSTR =>
						when cJMP_INSTR =>
						when others =>
					end case;
					
			-- output values for EXEC state
			when EXEC =>
			
					-- Regardless of the instruction		
					rwmem_o <= cREAD;
					rwreg_o <= cREAD; 
					ldres_o <= '1';
					ldpc_o  <= '1';
					ldir_o  <= '0';	

					-- Depending on each instruction						
					case opcode_i is
						when cLOAD_INSTR =>
						when cSTORE_INSTR =>
						when cMOVE_INSTR =>
						when cADD_INSTR =>					
						when cSHIFTL_INSTR =>
						when cCMP_INSTR =>
						when cBR_INSTR =>
							if flag_i = '1' then
								selpc_o <= '1';
							else
								selpc_o <= '0';
							end if;
						when cJMP_INSTR =>
							selpc_o <= '1';
						when OTHERS =>
					end case;
			
			-- output values for MEM_ACCESS state
			when MEM_ACCESS =>
			
					-- Regardless of the instruction		
					rwmem_o <= cREAD;
					rwreg_o <= cREAD; 
					ldres_o <= '0';
					ldpc_o  <= '0';
					ldir_o  <= '0';		

					-- Depending on each instruction						
					case opcode_i is
						when cLOAD_INSTR =>
						when cSTORE_INSTR =>
							oealu_o <= '1';
							rwmem_o <= cWRITE;
						when cMOVE_INSTR =>
						when cADD_INSTR =>
						when cSHIFTL_INSTR =>
						when cCMP_INSTR =>
						when cBR_INSTR =>
						when cJMP_INSTR =>
						when OTHERS =>
					end case;
			
			-- output values for WRITE_BACK state			
			when WRITE_BACK =>
			
					-- Regardless of the instruction		
					rwmem_o <= cREAD;
					rwreg_o <= cREAD; 
					ldres_o <= '0';
					ldpc_o  <= '0';
					ldir_o  <= '0';	

					-- Depending on each instruction				
					case opcode_i is
						when cLOAD_INSTR =>
							rwreg_o <= cWRITE;			
						when cSTORE_INSTR =>
							oealu_o <= '0';
						when cMOVE_INSTR =>
							rwreg_o <= cWRITE;	
						when cADD_INSTR =>
							rwreg_o <= cWRITE;
						when cSHIFTL_INSTR =>
							rwreg_o <= cWRITE;	
						when cCMP_INSTR =>
						when cBR_INSTR =>
						when cJMP_INSTR =>
						when OTHERS =>
					end case;

			-- Otherwise false state. Do nothing
			when OTHERS =>
	
		end case;	
	end process ProcessOutputLogic;

end Behavioral;

