----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    16:09:36 05/02/2018 
-- Design Name: 
-- Module Name:    alu - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.miniproc_pkg.all;

entity alu is
	port (
		clk_i   : in std_logic;
		reset_i : in std_logic;
		
		inalu_i  : in std_logic;
		ldres_i  : in std_logic;
		opalu_i  : in std_logic_vector(1 downto 0);
		
		a_i      : in std_logic_vector(7 downto 0);
		b_i      : in std_logic_vector(7 downto 0);
		
		carry_o  : out std_logic;
		flag_o   : out std_logic;
		result_o : out std_logic_vector(7 downto 0)
	);
end alu;

architecture Behavioral of alu is

	--Signals
	signal result_arith_s : std_logic_vector(7 downto 0);
	signal b_s : std_logic_vector(7 downto 0);
	signal flag_s : std_logic;
	
begin

	--Code
	
	--Selection of "B" or "00000000"
	b_s <= b_i when inalu_i = '1' else 
			 "00000000" when inalu_i = '0';	
	
	--Arithmetic Logic Unit
   process(a_i, b_s, opalu_i)
	begin
		case opalu_i is
			when cADD => 
				result_arith_s <= std_logic_vector( signed(a_i) + signed(b_s) ); 
				
			when cSHIFTL => 
				result_arith_s <= std_logic_vector( shift_left(signed(a_i), 1) ); 
				
			when cCMP =>
				if to_integer(signed(a_i)) < to_integer(signed(b_s)) then
					flag_s <= '1';
				else 
					flag_s <= '0';
				end if;
					
		   when others => 
				result_arith_s <= "XXXXXXXX";
				
		end case;
	end process;
	
	--Transmits the output value and flag
	process(clk_i, reset_i)
	begin
		if reset_i = '1' then
			result_o <= "00000000";
		elsif rising_edge(clk_i) then
			if ldres_i = '1' then
				flag_o <= flag_s;
				result_o <= result_arith_s;
			end if;
		end if;
	end process;
	
end Behavioral;

